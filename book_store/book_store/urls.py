from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    path('admin/', admin.site.urls),
    path('sign_up/', include('rest_auth.registration.urls')),
    path('', include('rest_auth.urls')),
    path('', include('core.urls')),
    *static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
]

urlpatterns += staticfiles_urlpatterns()
