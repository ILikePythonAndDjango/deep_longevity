from django.contrib.auth.models import AbstractUser
from django.db.models.expressions import F
from django.db import transaction, models
from django.core import exceptions


class User(AbstractUser):

    bill = models.PositiveIntegerField(default=0)


class Book(models.Model):

    title = models.CharField(max_length=100)
    epilogue = models.TextField()
    author = models.CharField(max_length=100)
    added = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey(User, blank=True, on_delete=models.CASCADE)
    price = models.PositiveIntegerField()
    in_stock = models.PositiveIntegerField()

    def __str__(self):
        return self.title


class Good(models.Model):

    '''
    It'd be a pragmatic desicion to place ManyToMany relationship like class
    instead of field in the Book class.
    '''

    subscriber = models.ForeignKey(User, on_delete=models.CASCADE)
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    subscribed_on = models.DateTimeField(auto_now_add=True)
    is_sold = models.BooleanField(default=False)

    @transaction.atomic
    def to_sell(self):
        ''' Shortcut placed this to mark sold books '''
        if (self.book.in_stock - 1) < 0:
            raise exceptions.ValidationError("Good aren't in stock")
        if (self.subscriber.bill - self.book.price) < 0:
            raise exceptions.ValidationError('Pay bills')
        self.is_sold = True
        self.subscriber.bill = F('bill') - self.book.price
        self.subscriber.save()
        self.book.in_stock = F('in_stock') - 1
        self.book.save()
        self.save()
        # must have chaining due to Django ORM architecture
        return self

    class Meta:
        db_table = 'book_store'
        unique_together = ('subscriber', 'book', 'subscribed_on')
