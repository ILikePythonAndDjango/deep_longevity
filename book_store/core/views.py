from django.db.models.expressions import F
from django.utils import timezone
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework import viewsets, status
from . import serializers
from .permissions import IsSubscribed
from .models import Good, Book, User


class UserViewSet(viewsets.ReadOnlyModelViewSet):

    """
    You cannot sign up via `/users/` endpoint.
    """

    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer
    permission_classes = (IsAuthenticated,)

    def get_serializer_class(self):
        if self.action == 'pay_bills':
            return serializers.BillSerializer
        return super().get_serializer_class()

    @action(detail=True, methods=('put',))
    def pay_bills(self, request, pk=None):
        ''' you can only increase '''
        user = self.get_object()
        user.bill = F('bill') + request.data.get('bill', 0)
        user.save()
        return Response({'status': 'ok'})


class BookViewSet(viewsets.ModelViewSet):

    queryset = Book.objects.all()
    serializer_class = serializers.BookSerializer
    permission_classes = (IsAuthenticated, IsSubscribed)

    def perform_save(self, serializer):
        serializer.save(owner=self.request.user)

    @action(detail=True, methods=('post',))
    def subscribe(self, request, pk=None):
        try:
            Good.objects.create(
                subscriber=request.user, book=self.get_object())
        except Exception as e:
            return Response(str(e), status=status.HTTP_400_BAD_REQUEST)
        return Response({'status': 'ok'})

    @action(detail=True, methods=('post',))
    def buy(self, request, pk=None):
        good = Good.objects.filter(
            subscriber=request.user, book=self.get_object()).first()
        try:
            good.to_sell()
        except Exception as e:
            return Response(
                {'user_id': request.user.id, 'status': 'wrong', 'msg': str(e)},
                status=status.HTTP_400_BAD_REQUEST,
            )
        return Response(
            {
                'user_id': request.user.id,
                'status': 'ok',
                'period': str(timezone.now())
            }
        )
