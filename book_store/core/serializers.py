from rest_framework import serializers
from .models import User, Book


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = (
            'id',
            'username',
            'email',
            'first_name',
            'last_name',
            'bill'
        )

class BillSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('bill',)


class BookSerializer(serializers.ModelSerializer):

    class Meta:
        model = Book
        fields = (
            'id',
            'title',
            'epilogue',
            'author',
            'added',
            'owner',
            'price',
            'in_stock',
        )
