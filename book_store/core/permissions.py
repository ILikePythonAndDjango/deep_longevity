from rest_framework import permissions
from . import models

class IsOwnerOrReadOnly(permissions.IsAuthenticated):

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True

        return obj.owner == request.user

class IsSubscribed(permissions.IsAuthenticated):

    def has_object_permission(self, request, view, obj):
        return view.action == 'subscribe' or models.Good.objects.filter(
            subscriber=request.user, book=obj).exists()
