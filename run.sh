#!/usr/bin/env bash

cd book_store

python3 manage.py makemigrations
python3 manage.py migrate
# python3 manage.py collectstatic --noinput
python3 -m gunicorn 'book_store.wsgi' \
  --config=/app/etc/gunicorn_configuration.py
